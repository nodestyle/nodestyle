package nodestyle.filesystem;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileSystem {
	
	/**
	 * Reads a file from the path specified and returns a string containing the contents of that file.
	 * @param String path
	 * @return String if file read was successful, and null if not.
	 */
	public static String readFile(String path) {
		try {
			Path p = Paths.get(path);
			return new String(Files.readAllBytes(p));
		} catch(Exception e) { return null; }
	}
	
	/**
	 * Reads a file from the path specified and returns a string containing the contents of that file.
	 * @param Path path
	 * @return String if file read was successful, and null if not.
	 */
	public static String readFile(Path path) {
		try {
			Path p = Paths.get(path.toString());
			return new String(Files.readAllBytes(p));
		} catch(Exception e) { return null; }
	}
	
	/**
	 * Writes to a file located at the path specified.
	 * @param String contents
	 * @param String path
	 */
	public static void writeFile(String contents, String path) {
	    try {
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(path));
	    	writer.write(contents);
		    writer.close();
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	/**
	 * Writes to a file located at the path specified.
	 * @param String contents
	 * @param Path path
	 */
	public static void writeFile(String contents, Path path) {
	    try {
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()));
	    	writer.write(contents);
		    writer.close();
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	}
	
}
